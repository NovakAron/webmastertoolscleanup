# Google Webmaster Bulk URL Removal
-----------------------------------

## Requirements
* Python 2.7.x
* Selenium bindings for Python
* Selenium Standalone Server - http://www.seleniumhq.org/download/

# Installing Selenium bindings for Python
[sudo] easy_install selenium or [sudo] pip install selenium

## Running a test
 * Launch Selenium Standalone Server [java -jar selenium-server-standalone...jar]
 * edit username / password and site in the WebmasterToolsCleanup.py
 * list the URLs to remove in remove-urls.txt
 * [python WebmasterToolsCleanup.py]

