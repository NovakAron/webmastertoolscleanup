""" Webmaster Tools Cleanup
    Removes multiple URLs automatically from Google Index
    It requires you that the site is already added to
    Webmaster Tools. See setUp() for the configuration options.
"""

from selenium import selenium
import unittest, time, sys, traceback

class WebmasterToolsCleanup(unittest.TestCase):
    """A helper class to remove many URLs at once with selenium,
       see http://seleniumhq.org/ - Selenium Server
    """
  
    def setUp(self):
        """Makes the initialization of the selenium class,
           hold configurable variables
        """
        
        """ This file must contain valid URLs, one per line,
            those what you'd like to remove.
            Google has a hard limit of 1000 URLs so it does not
            worth to specify more in the file.
        """
        self.url_file = 'remove-urls.txt'
        """
            Access credentials to the Google account
        """
        self.google_login = "example@gmail.com"
        self.google_pw = "example"
        """
            The name of the validated site
            as it appears as a link under
            Webmaster Tools dashboard
        """
        self.target_site_url = "www.example.com"
        
        self.target_host = "www.google.com/webmasters/"
        self.target_ip = "127.0.0.1"
        self.browser = "*firefox"
        self.verrors = []
        self.selenium = selenium(
          self.target_ip,
          4444,
          self.browser,
          "http://" + self.target_host + "/"
        )
        self.selenium.start()
  
    def tearDown(self):
        """Cleans up selenium"""
        self.selenium.stop()
        self.assertEqual([], self.verrors)
        
    def testRemoveURLs(self):
        """Opens remove-urls.txt and removes them via google webmaster tools"""
        inputfile = open(self.url_file)
        urls = inputfile.readlines()
        inputfile.close()

        self.selenium.open("/webmasters/")
        self.strong_wait()
        self.strong_click("link=Sign in to Webmaster Tools")
        self.strong_wait()
        self.strong_type("id=Email", self.google_login)
        self.strong_type("id=Passwd", self.google_pw)
        self.strong_click("id=PersistentCookie")
        self.strong_click("id=signIn")
        self.strong_wait()
        time.sleep(5)
        self.strong_click("link=" + self.target_site_url)
        self.strong_wait()
        
        for url in urls:
            self.strong_click("link=Remove URLs")
            self.strong_wait()
            try:
                self.strong_type("id=urlt", url)
                self.strong_click("name=urlt.submitButton")
                self.strong_wait()
                self.strong_click("id=submit-button")
                self.strong_wait()
            except:
                print "could not add " + url
        

    def __jquery_selector(self, selector):
        """Converts id= selector to jquery selector"""
        return selector.replace("id=", "#").replace("css=", "")
        
    def strong_type(self, selector, val):
        """Types a text to an item"""
        try:
            self.selenium.focus(selector)
        except:
            print "go forward"
        self.selenium.type(selector, val)
        try:
            self.selenium.run_script(
                "$('" + self.__jquery_selector(selector) + "').change()"
            )
            self.selenium.focus("css=body")
        except:   
            print "go forward"
        
    def strong_click(self, selector):
        """Clicks on an item"""
        time.sleep(0.2)
        if (selector.find("link=") != -1):
            try:
                print "trying to rewrite"
                self.strong_click(
                  "//a[text()='" + selector.replace("link=", "") + "']"
                )
                return
            except:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_tb(exc_traceback, file=sys.stdout)
                print "go forward"

        try:
            self.selenium.focus(selector)
        except:
            print "go forward"
        self.selenium.click(selector)

    def strong_wait(self):
        #"""Waits in a robust way"""
        try:
            self.selenium.wait_for_page_to_load("30000")
        except:
            time.sleep(10)
            print "go forward"

if __name__ == "__main__":
    unittest.main()
